/**
 * 
 */
var app = angular.module("myApp", ["ngRoute", "ngStorage"]);

//Route table
app.config(function ($routeProvider, $locationProvider){
	$locationProvider.hashPrefix('');
	$routeProvider
	.when('/customer-search',{
		templateUrl: 'html/customer-search.html',
		controller: 'customerSearchCtrl'
	}).when('/customer-search/:query/:category/:type/:condition/:shooter/:rpg/:adventure/:racing',{
		templateUrl: 'html/customer-search.html',
		controller: 'customerSearchCtrl'
	})
	.when('/add-product',{
		templateUrl: 'html/add-product.html',
		controller: 'addProductCtrl'
	})
	.when('/admin-search',{
		templateUrl: 'html/admin-search.html',
		controller: 'adminSearchCtrl'
	})
	.when('/admin-search/:query',{
		templateUrl: 'html/admin-search.html',
		controller: 'adminSearchCtrl'
	}).when('/admin-search/:query/:category/:type/:condition/:shooter/:rpg/:adventure/:racing',{
		templateUrl: 'html/admin-search.html',
		controller: 'adminSearchCtrl'
	})
	.when('/update-product/:id',{
		templateUrl: 'html/update-product.html',
		controller: 'updateProductCtrl'
	})
	.when('/view-product/:id',{
		templateUrl: 'html/view-product.html',
		controller: 'viewProductCtrl'
	})	
	.when('/product-inspector/:id',{
		templateUrl: 'html/product-inspector.html',
		controller: 'productInspectorCtrl'
	})
	.when('/cart',{
		templateUrl: 'html/cart.html',
		controller: 'cartCtrl'
	})	
	.when('/cart/:id',{
		templateUrl: 'html/cart.html',
		controller: 'cartCtrl'
	})	
	.when('/about',{
		templateUrl: 'html/about.html',
	})	
	.when('/',{
		templateUrl: 'html/customer-search.html',
		controller: 'customerSearchCtrl'
	})
});

//Controller for customer-search.html
app.controller("customerSearchCtrl", function($scope, $http, $routeParams, $location)
{
	//Variable declaration
	$scope.products = [];
	$scope.query = $routeParams.query == 'undefined' || $routeParams.query == undefined ? '' : $routeParams.query;
	$scope.category = $routeParams.category == undefined ? 'any' : $routeParams.category;
	$scope.type = $routeParams.type == undefined ? 'any' : $routeParams.type;
	$scope.condition = $routeParams.condition == undefined ? 'any' : $routeParams.condition;
	$scope.shooter = $routeParams.shooter == undefined ? false : $routeParams.shooter;
	$scope.rpg = $routeParams.rpg == undefined ? false : $routeParams.rpg;
	$scope.adventure = $routeParams.adventure == undefined ? false : $routeParams.adventure;
	$scope.racing = $routeParams.racing == undefined ? false : $routeParams.racing;
	
	//A request for retrieving product list from CustomerServlet
	$http({
		method: "GET",
		url: "CustomerServlet?query=" + $scope.query + "&category=" + $scope.category + "&condition=" + $scope.condition + "&type=" + $scope.type + "&shooter=" + $scope.shooter + 
		"&rpg=" + $scope.rpg + "&adventure=" + $scope.adventure + "&racing=" + $scope.racing
	}).then(function successCallback(response) {
		$scope.products = response.data;
	}, function errorCallback(response) {
		$(".message").html("Server Error");
	});
	
	$scope.submit = function() {
		$location.path('/customer-search/' +  ($scope.query == '' ? undefined : $scope.query) + '/' + $scope.category + '/' + $scope.type + '/' + $scope.condition + '/' + $scope.shooter + '/' + $scope.rpg + '/' + $scope.adventure + '/' + $scope.racing);
	};
	
	$scope.goToLink = function(product) {
		$location.path('/view-product/' + product.idProduct);
	};
});
//http://localhost:8080/Website-2017-Nov/CustomerServlet?query=asd&category=Xbox One&condition=new&type=device&shooter=false&rpg=false&adventure=false&racing=false
//Controller for add-product.html
app.controller("addProductCtrl", function($scope, $http)
{
	//Hide message div
	$(".message").hide();
	$scope.product = null;
	
	//A function  to send a product to AdminServlet
	$("#addProductForm").submit(function(e){
		e.preventDefault();
		//Data checks
		if($scope.product.name.length > 100){
			$(".message").html("Product name is too long");
			$(".message").show();
			return;
		}
		//Price data checks
		//-----------------------------------------------------
		if(isNaN($scope.product.price)){
			$(".message").html("Product price is not a number");
			$(".message").show();
			return;
		}
		if($scope.product.price.indexOf(".") != -1){
			if($scope.product.price.split('.')[0].length > 10) {
				$(".message").html("Product price cannot have more than 10 digits");
				$(".message").show();
				return;
			}
			if($scope.product.price.split('.')[1].length > 2){
				$(".message").html("Product price cannot have more than 2 decimal places");
				$(".message").show();
				return;
			}
		}else{
			if($scope.product.price.length > 10){
				$(".message").html("Product price cannot have more than 10 digits");
				$(".message").show();
				return;
			}
		}
		//Stock data checks
		//-----------------------------------------------------
		if(isNaN($scope.product.stock)){
			$(".message").html("Product stock is not a number");
			$(".message").show();
			return;
		}
		if($scope.product.stock.indexOf(".") != -1){
			$(".message").html("Product stock cannot have decimal places");
			$(".message").show();
			return;
		}
		if($scope.product.stock.length > 6){
			$(".message").html("Product stock cannot have more than 6 digits");
			$(".message").show();
			return;
		}
		//-----------------------------------------------------
		if($scope.product.description.length > 5000){
			$(".message").html("Product description is too long");
			$(".message").show();
			return;
		}
		if($scope.product.categoryOf == null){
			$(".message").html("Category cannot be undefined");
			$(".message").show();
			return;
		}
		if($scope.product.typeOf == null){
			$(".message").html("Type cannot be undefined");
			$(".message").show();
			return;
		}
		if($scope.product.conditionOf == null){
			$(".message").html("Condition cannot be undefined");
			$(".message").show();
			return;
		}
		
        // Get form
        var form = $('#addProductForm')[0];
        var data = new FormData(form);
        //Request
		$.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "AdminServlet",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
            	window.location.href = "http://localhost:8080/Website-2017-Nov/index.html#/product-inspector/" + data;
            },
            error: function (e) {
                $(".message").html("Server error");
                $(".message").show();
            }
	   });
	});	
});

//Controller for admin-search.html
app.controller("adminSearchCtrl", function($scope, $http, $routeParams, $location)
{
	//Variable declaration
	$scope.products = [];
	$scope.query = $routeParams.query == 'undefined' || $routeParams.query == undefined ? '' : $routeParams.query;
	$scope.category = $routeParams.category == undefined ? 'any' : $routeParams.category;
	$scope.type = $routeParams.type == undefined ? 'any' : $routeParams.type;
	$scope.condition = $routeParams.condition == undefined ? 'any' : $routeParams.condition;
	$scope.shooter = $routeParams.shooter == undefined ? false : $routeParams.shooter;
	$scope.rpg = $routeParams.rpg == undefined ? false : $routeParams.rpg;
	$scope.adventure = $routeParams.adventure == undefined ? false : $routeParams.adventure;
	$scope.racing = $routeParams.racing == undefined ? false : $routeParams.racing;
	
	//A request to retrieve product list from AdminServlet
	$http({
		method: "GET",
		url: "AdminServlet?query=" + $scope.query + "&category=" + $scope.category + "&condition=" + $scope.condition + "&type=" + $scope.type + "&shooter=" + $scope.shooter + 
		"&rpg=" + $scope.rpg + "&adventure=" + $scope.adventure + "&racing=" + $scope.racing
	}).then(function successCallback(response) {
		$scope.products = response.data;
	}, function errorCallback(response) {
		$(".message").html("Server Error");
	});
	
	//A function to delete a product with AdminServlet
	$scope.deleteReal = function(id){
		//Request  
		$http({
			method: "DELETE",
			url: "AdminServlet?id=" + id
		}).then(function(response){
			alert(response.data);
		});
	};
	
	$scope.submit = function() {
		$location.path('/admin-search/' +  ($scope.query == '' ? undefined : $scope.query) + '/' + $scope.category + '/' + $scope.type + '/' + $scope.condition + '/' + $scope.shooter + '/' + $scope.rpg + '/' + $scope.adventure + '/' + $scope.racing);
	};
	
	$scope.goToLink = function(product) {
		$location.path('/product-inspector/' + product.idProduct);
	};
});
//http://localhost:8080/Website-2017-Nov/AdminServlet?query=asd&category=Xbox One&condition=new&type=device&shooter=false&rpg=false&adventure=false&racing=false
//Controller for update-product.html
app.controller("updateProductCtrl", function($scope, $http, $routeParams, $location)
{
	//Hide message div
	$(".message").hide();
	$scope.product = null;
	
	//A request to retrieve a single product from AdminServlet
	$http({
			method: "GET",
			url: "AdminServlet?id="+$routeParams.id,
		}).then(function(response){
			$scope.product = response.data;
		});
	  
    //A function to update a product through AdminServlet
    $scope.update = function(){
    	
    	$scope.product.price = $scope.product.price.toString();
    	$scope.product.stock = $scope.product.stock.toString();
    	
		//Data checks
		if($scope.product.name.length > 100){
			$(".message").html("Product name is too long");
			$(".message").show();
			return;
		}
		//Price data checks
		//-----------------------------------------------------
		if(isNaN($scope.product.price)){
			$(".message").html("Product price is not a number");
			$(".message").show();
			return;
		}
		if($scope.product.price.indexOf(".") != -1){
			if($scope.product.price.split('.')[0].length > 10) {
				$(".message").html("Product price cannot have more than 10 digits");
				$(".message").show();
				return;
			}
			if($scope.product.price.split('.')[1].length > 2){
				$(".message").html("Product price cannot have more than 2 decimal places");
				$(".message").show();
				return;
			}
		}else{
			if($scope.product.price.length > 10){
				$(".message").html("Product price cannot have more than 10 digits");
				$(".message").show();
				return;
			}
		}
		//Stock data checks
		//-----------------------------------------------------
		if(isNaN($scope.product.stock)){
			$(".message").html("Product stock is not a number");
			$(".message").show();
			return;
		}
		if($scope.product.stock.indexOf(".") != -1){
			$(".message").html("Product stock cannot have decimal places");
			$(".message").show();
			return;
		}
		if($scope.product.stock.length > 6){
			$(".message").html("Product stock cannot have more than 6 digits");
			$(".message").show();
			return;
		}
		//-----------------------------------------------------
		if($scope.product.description.length > 5000){
			$(".message").html("Product description is too long");
			$(".message").show();
			return;
		}
		if($scope.product.categoryOf == null){
			$(".message").html("Category cannot be undefined");
			$(".message").show();
			return;
		}
		if($scope.product.typeOf == null){
			$(".message").html("Type cannot be undefined");
			$(".message").show();
			return;
		}
		if($scope.product.conditionOf == null){
			$(".message").html("Condition cannot be undefined");
			$(".message").show();
			return;
		}
		
    	// Get form
		var form = $('#updateProductForm')[0];
		var data = new FormData(form);
		//Request    
		$.ajax({
	        type: "PUT",
	        enctype: 'multipart/form-data',
	        url: "AdminServlet",
	        data: data,
	        processData: false,
	        contentType: false,
	        cache: false,
	        timeout: 600000,
	        success: function (data) {
	        	window.location.href = "http://localhost:8080/Website-2017-Nov/index.html#/product-inspector/" + $scope.product.idProduct;
	        },
	        error: function (e) {
	            $(".message").html("Server Error");
	        }
	    });
    };
});
//Controller for view-product.html
app.controller('viewProductCtrl', function($scope, $http, $routeParams, $location){
	
	$scope.product = null;

	$http({
		method: "get",
		url: "CustomerServlet?id=" + $routeParams.id
	}).then(function successCallback(response) {
		$scope.product = response.data;
	}, function errorCallback(response) {
		$(".message").html("Server Error");
	});
	
	//A function to add product to cart
	$scope.addToCart = function(id){
		if(!sessionStorage.getItem(id)){
			sessionStorage.setItem(id, '1');
		}else{
			var quantity = Number(sessionStorage.getItem(id));
			if(quantity < 10){
				sessionStorage.setItem(id, parseInt(quantity) + 1);
			}
		}
		$location.path('/cart');
	};
});
//For testing purposes:
//http://localhost:8080/Website-2017-Nov/CustomerServlet?id=23

//Controller for product-inspector.html
app.controller('productInspectorCtrl', function($scope, $http, $routeParams){
	
	$scope.product = null;

	$http({
		method: "get",
		url: "AdminServlet?id=" + $routeParams.id
	}).then(function successCallback(response) {
		$scope.product = response.data;
	}, function errorCallback(response) {
		$(".message").html("Server Error");
	});
});

//Controller for cart.html
app.controller('cartCtrl', function($scope, $http, $routeParams, $location){
	alert(Object.keys(sessionStorage));
	$scope.quantityMap = sessionStorage;
	
	$http({
		method: "get",
		url: "CartServlet?idArray=" + Object.keys(sessionStorage)
	}).then(function successCallback(response) {
		$scope.products = response.data;
	}, function errorCallback(response) {
		$(".message").html("Server Error");
	});
	
	$scope.goToLink = function(product) {
		$location.path('/view-product/' + product.idProduct);
	};
	
	$scope.cartRemove = function(id){
		sessionStorage.removeItem(id);
	};
	
	$scope.increment = function(id)
	{
		var quantity = Number($scope.quantityMap.getItem(id));
		if(quantity < 10){
			$scope.quantityMap.setItem(id, parseInt(quantity) + 1);
			sessionStorage = $scope.quantityMap;
		}
	};
	
	$scope.decrement = function(id)
	{
		var quantity = Number($scope.quantityMap.getItem(id));
		if(quantity > 1){
			$scope.quantityMap.setItem(id, parseInt(quantity) - 1);
			sessionStorage = $scope.quantityMap;
		}
	};
});


