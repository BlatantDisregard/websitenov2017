/**
 * 
 */

$(document).ready(function(){
	
	//A function to show files to be uploaded
	$('.image-input').on('change', function(e){
		var files = e.target.files;
		$('.images-to-upload').html('');
		$.each(files, function(i, file){
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function(e){
				var template = '<img src='+e.target.result+'>';
				$('.images-to-upload').append(template);
			};
		});
	});
});