package com.srk.pkg;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class CartServlet
 */
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private JdbcUtility jdbc = new JdbcUtility();
	private ServletUtility servletUtility = new ServletUtility();
	private Gson gson = new Gson();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		
		//Variable declaration
		PrintWriter out = response.getWriter();
		ArrayList<Product> products = null;
		String idArray = request.getParameter("idArray");
			
		try{
			products = jdbc.findProductsById(idArray);
		}catch(Exception ex){
			ex.printStackTrace(out);
		}
		
		if(products != null && !products.isEmpty())
			for(Product p: products)
			{
				String imageName = servletUtility.getProfileImage(p.getIdProduct(), out);
				p.setProfileImage(imageName);
			}
		out.println(gson.toJson(products));

		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
