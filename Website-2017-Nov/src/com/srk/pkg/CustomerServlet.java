package com.srk.pkg;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Servlet implementation class CustomerServlet
 */
public class CustomerServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    private JdbcUtility jdbc = new JdbcUtility();
    private Gson gson = new Gson();
	private ServletUtility servletUtility = new ServletUtility();

	   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");

		//Variable declaration
		PrintWriter out = response.getWriter();
		ArrayList<Product> products = null;
		Product product = null;
		String id = request.getParameter("id");
				
		if(id == null){
			String query = request.getParameter("query");
			String category = request.getParameter("category");
			String type = request.getParameter("type");
			String condition = request.getParameter("condition");
			String tagsShooter = request.getParameter("shooter");
			String tagsRpg = request.getParameter("rpg");
			String tagsAdventure = request.getParameter("adventure");
			String tagsRacing = request.getParameter("racing");
			
			try{
				products = jdbc.advancedSearch(query, category, type, condition, tagsShooter, tagsRpg, tagsAdventure, tagsRacing);
			}catch(Exception ex){
				ex.printStackTrace(out);
			}
		}else{//Finding and returning ONE product	
			try{
				product = jdbc.findProduct(Integer.parseInt(id));
				List<String> images = servletUtility.getImages(Integer.parseInt(id),out);
				product.setImages(images);	
				product.setTags(jdbc.findTags(Integer.parseInt(id)));
			}catch(Exception ex){
				ex.printStackTrace(out);
			}
		}
		//Output
		if(products != null){
			for(Product p: products)
			{
				String imageName = servletUtility.getProfileImage(p.getIdProduct(), out);
				p.setProfileImage(imageName);
			}
			out.println(gson.toJson(products));
		}else{
			product.setProfileImage(product.getImages().size() > 0 ? product.getImages().get(0) : null);
			out.println(gson.toJson(product));
		}
		
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
