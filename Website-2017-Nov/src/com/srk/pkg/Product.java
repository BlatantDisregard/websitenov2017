package com.srk.pkg;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
public class Product {
	
	private int idProduct;
	private String name;
	private String description;
	private float price;
	private int stock;
	private String categoryOf;
	private String typeOf;
	private String conditionOf;
	private java.sql.Date creationDate;
	private String profileImage;
	private List<String> images;
	private List<String> tags;
	
	public Product(int idProduct, String name, String description, float price, int stock, String categoryOf,  String typeOf, String conditionOf, java.sql.Date creationDate)
	{
		this.idProduct = idProduct;
		this.name = name;
		this.description = description;
		this.price = price;
		this.stock = stock;
		this.categoryOf = categoryOf;
		this.typeOf = typeOf;
		this.conditionOf = conditionOf;
		this.creationDate = creationDate;
	}
	
	public Product(String name, String description, float price, int stock, String categoryOf,  String typeOf, String conditionOf)
	{
		this.name = name;
		this.description = description;
		this.price = price;
		this.stock = stock;
		this.categoryOf = categoryOf;
		this.typeOf = typeOf;
		this.conditionOf = conditionOf;
	}
	
	public Product(int idProduct, String name, String description, float price, int stock, String categoryOf,  String typeOf, String conditionOf)
	{
		this.idProduct = idProduct;
		this.name = name;
		this.description = description;
		this.price = price;
		this.stock = stock;
		this.categoryOf = categoryOf;
		this.typeOf = typeOf;
		this.conditionOf = conditionOf;
	}
	
	public Product(int idProduct, String name, String description, float price, int stock, String categoryOf,  String typeOf, String conditionOf, java.sql.Date creationDate, ArrayList<String> tags)
	{
		this.idProduct = idProduct;
		this.name = name;
		this.description = description;
		this.price = price;
		this.stock = stock;
		this.categoryOf = categoryOf;
		this.typeOf = typeOf;
		this.conditionOf = conditionOf;
		this.creationDate = creationDate;
		this.tags = tags;
	}
	
	//getters
	public int getIdProduct() {return idProduct;}
	public String getName() {return name;}
	public String getDescription() {return description;}
	public java.sql.Date getCreationDate(){return creationDate;}
	public float getPrice() {return price;}
	public int getStock() {return stock;}
	public List<String> getTags() {return tags;}
	public List<String> getImages() {return images;}
	public String getCategory(){return categoryOf;}
	public String getType(){return typeOf;}
	public String getCondition(){return conditionOf;}
	public String getProfileImage() {return profileImage;}
	
	//setters
	public void setTags(List<String>  tags) {this.tags = tags;}
	public void setImages(List<String>  images) {this.images = images;}
	public void setProfileImage(String profileImage) {this.profileImage = profileImage;}
}
