package com.srk.pkg;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

public class ServletUtility {
	
	private JdbcUtility jdbc = new JdbcUtility();
	
	//A method for parsing html requests into a FileItem list
	protected List<FileItem> getFormItems(HttpServletRequest request, PrintWriter out)
	{
	    //int maxFileSize = 50 * 1024;
	   // int maxMemSize = 4 * 1024;
		
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		//Maximum size to be stored in memory
		//factory.setSizeThreshold(maxMemSize);
		
		//Create new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		//Maximum file size to be uploaded
		//upload.setSizeMax(maxFileSize);
		
		List<FileItem> formItems = null;
		
		try{
			//Parse the request to get file items
			formItems = upload.parseRequest(new ServletRequestContext(request));
		}catch(Exception ex){
			out.println("Error reading form content</br>");	
			ex.printStackTrace(out);
			out.println("</br></br>");
		}
		
		return formItems;
	}
	
	//A method for saving images to server
	protected void saveFiles(List<FileItem> formItems, int productId, PrintWriter out, String filePath){
	    File file ;
		ArrayList<String> fileNames = new ArrayList<String>();
		
		try{
			for(FileItem fi: formItems){
				if(fi.getContentType() != null && fi.getContentType().contains("image")){
					//Get uploaded file parameters
					fileNames.add(fi.getName());
				}
			}
			
			Map<String, String> fileNameMap = null;
			try{
				fileNameMap = jdbc.addImages(productId, fileNames);
			}catch(Exception ex){
				out.println("Error adding image data to images table</br>");
				ex.printStackTrace(out);
				out.println("</br></br>");
			}

			
			for(FileItem fi: formItems){
				if(fi.getContentType() != null && fi.getContentType().contains("image")){
					//Get uploaded file parameters
					String fileName = fi.getName();
					
					//Write the file
					if(fileName.lastIndexOf("\\") >= 0){
						file = new File(filePath + "\\" + productId + "-" + fileNameMap.get(fileName.substring( fileName.lastIndexOf("\\"))));
					}else{
						file = new File(filePath + "\\" + productId + "-" + fileNameMap.get(fileName.substring(fileName.lastIndexOf("\\") + 1)));
					}
					fi.write(file);
				}	
			}
		}catch(Exception ex){
			out.println("Error saving images to server</br>");
			ex.printStackTrace(out);
			out.println("</br></br>");
		}	
	}
	//Delete files based on file name
	protected void deleteFiles(List<FileItem>formItems, String productId,PrintWriter out, String filePath)
	{
		ArrayList<String> images = new ArrayList<String>();
		Map<String, Object> formMap = new HashMap<String,Object>();
		try{				
			for (FileItem item : formItems) {
			    if (item.isFormField()) {
			        if(item.getFieldName().substring(0, productId.length()).equals(productId))
			        {
			        	images.add(item.getString());
			        }
			    }
			}
		}catch(Exception ex){
			out.println("Error reading form content</br>");	
			ex.printStackTrace(out);
			out.println("</br></br>");
		}
		ArrayList<String> imageIds = new ArrayList<String>();
		for(String s : images)
		{
			File file = new File(filePath+"\\"+s);
			if(file.delete())
			{
				imageIds.add(s.substring(productId.length()+1, s.length()));
			}
		}
		try{
			jdbc.deleteImagesByImageId(imageIds);
		}catch(Exception ex){
			ex.printStackTrace(out);
		}
	}
	
	//Delete files based on product id
	protected void deleteAllFiles(String productId,PrintWriter out, String filePath)
	{
		ArrayList<String> images = new ArrayList<String>();
		File imageFolder = new File(filePath);
		try{
			for(File f : imageFolder.listFiles())
			{
				if(f.getName().substring(0, f.getName().indexOf("-")).equals(productId)){
					f.delete();
				}
			}
		}catch(Exception ex){
			ex.printStackTrace(out);
		}
	}
	
	//A method for adding product and tags to database
	protected int addProduct(List<FileItem> formItems, PrintWriter out)
	{
		int productId = 0;
		ArrayList<String> tags = new ArrayList<String>();
		Map<String, Object> formMap = new HashMap<String,Object>();
		try{		
			formMap = new HashMap<String, Object>();
			
			for (FileItem item : formItems) {
			    if (item.isFormField()) {
			        if(item.getFieldName().substring(0,3).equals("tag"))
			        {
			        	tags.add(item.getString());
			        }else
			        	formMap.put(item.getFieldName(), item.getString());
			    }
			}
		}catch(Exception ex){
			out.println("Error reading form content</br>");	
			ex.printStackTrace(out);
			out.println("</br></br>");
		}	
		Product product = new Product((String)formMap.get("name"), (String)formMap.get("description"), Float.parseFloat((String)formMap.get("price")), Integer.parseInt((String)formMap.get("stock")), (String)formMap.get("category"), (String)formMap.get("type"), (String)formMap.get("condition"));
		
		try{
			productId = jdbc.addProduct(product);
		}catch(Exception ex){
			out.println("Error adding product to product table</br>");	
			ex.printStackTrace(out);
			out.println("</br></br>");
		}
		
		try{
			jdbc.addTags(productId, tags);
		}catch(Exception ex){
			out.println("Error adding tags to tag table</br>");	
			ex.printStackTrace(out);
			out.println("</br></br>");
		}
		
		return productId;
	}
	
	//A method for updating product and tags in database
	protected int updateProduct(List<FileItem> formItems, PrintWriter out)
	{
		ArrayList<String> tags = new ArrayList<String>();
		Map<String, Object> formMap = new HashMap<String,Object>();
		try{		
			formMap = new HashMap<String, Object>();
			
			for (FileItem item : formItems) {
			    if (item.isFormField()) {
			        if(item.getFieldName().substring(0,3).equals("tag"))
			        { 
			        	tags.add(item.getString());
			        }else
			        	formMap.put(item.getFieldName(), item.getString());
			    }
			}
		}catch(Exception ex){
			out.println("Error reading form content</br>");	
			ex.printStackTrace(out);
			out.println("</br></br>");
		}
				
		Product product = new Product(Integer.parseInt((String)formMap.get("idProduct")),(String)formMap.get("name"), (String)formMap.get("description"), Float.parseFloat((String)formMap.get("price")), Integer.parseInt((String)formMap.get("stock")), (String)formMap.get("category"), (String)formMap.get("type"), (String)formMap.get("condition"));
		
		try{
			jdbc.updateProduct(product);
		}catch(Exception ex){
			out.println("Error adding product to product table</br>");	
			ex.printStackTrace(out);
			out.println("</br></br>");
		}
		
		try{
			jdbc.updateTags(product.getIdProduct(), tags);
		}catch(Exception ex){
			out.println("Error adding tags to tag table</br>");	
			ex.printStackTrace(out);
			out.println("</br></br>");
		}
		
		return product.getIdProduct();
	}
	
	//A method for loading images paths from server
	protected ArrayList<String> getImages(int productId, PrintWriter out){
		ArrayList<Integer> imageIds = null;
		try{
		imageIds = jdbc.findImages(productId);
		}catch(Exception ex){
			ex.printStackTrace(out);
		}
		
		ArrayList<String> imageNames = new ArrayList<String>();
		for(Integer i: imageIds)
		{
			imageNames.add(productId +"-"+i);
		}
		return imageNames;
	}
	//A method for loading profile image paths from server
	protected String getProfileImage(int productId, PrintWriter out){
		int imageId = 0;
		try{
		imageId = jdbc.findProfileImage(productId);
		}catch(Exception ex){
			ex.printStackTrace(out);
		}
		String imageName = productId +"-"+imageId;
		return imageName;
	}
}
