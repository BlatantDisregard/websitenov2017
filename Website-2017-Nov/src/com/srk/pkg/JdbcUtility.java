package com.srk.pkg;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JdbcUtility {
	
	private  Connection connection;
	private  Statement statement;
	private  PreparedStatement preparedStatement;
	
	//Connect to database
	private void connect() throws SQLException
	{
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/website-2017-nov-database", "root", "Qpalzmqpalzm1");
		}
		catch(Exception exc){
			System.out.println(exc);
		}
		statement = connection.createStatement();
	}
	//Close reading objects for database query calling
	public void disconnect() throws SQLException
	{
		if (preparedStatement != null) preparedStatement.close();
		if (statement != null) statement.close();
		if (connection != null) connection.close();
	}
	
	/* FOR ADDING
	 * --------------------------------------------------------------------------------------------------------
	 * FOR ADDING
	 */
	//A method for adding a product to database
	public int addProduct(Product product) throws SQLException
	{
		connect();
			
		preparedStatement = connection.prepareStatement("INSERT INTO products VALUES(?,?,?,?,?,?,?,?,?)");
		
		preparedStatement.setInt(1, 0);
		preparedStatement.setString(2, product.getName());
		preparedStatement.setString(3,  product.getDescription());
		preparedStatement.setFloat(4, product.getPrice());
		preparedStatement.setInt(5, product.getStock());
		preparedStatement.setString(6, product.getCategory());
		preparedStatement.setString(7, product.getType());
		preparedStatement.setString(8, product.getCondition());
		preparedStatement.setNull(9, 0);
		
		preparedStatement.executeUpdate();

		ResultSet rs = statement.executeQuery("SELECT last_insert_id()");
		rs.next();
		int lastid = rs.getInt(1);
		
		disconnect();
		
		return lastid;
	}
	//A method for adding tags to database
	public void addTags(int idProduct, ArrayList<String>tags) throws SQLException
	{
		connect();
		
		if(tags.isEmpty())
			return;
		
		preparedStatement = connection.prepareStatement("INSERT INTO tags VALUES(?,?,?,?,?)");
			
		preparedStatement.setInt(1, idProduct);
		
		//Set all to false
		preparedStatement.setBoolean(2, false);
		preparedStatement.setBoolean(3, false);
		preparedStatement.setBoolean(4, false);
		preparedStatement.setBoolean(5, false);
		
		//Set selected to true
		for(String s: tags){
			if(s.compareTo("shooter") == 0)preparedStatement.setBoolean(2, true);
			if(s.compareTo("rpg") == 0)preparedStatement.setBoolean(3, true);
			if(s.compareTo("action/adventure") == 0)preparedStatement.setBoolean(4, true);
			if(s.compareTo("racing") == 0)preparedStatement.setBoolean(5, true);
		}
		
		preparedStatement.executeUpdate();
		
		disconnect();
	}
	//A method for adding image details to database
	public Map<String, String> addImages(int idProduct, ArrayList<String> imageNames) throws SQLException
	{
		connect();
		
		Map<String, String> imageIds = new HashMap<String, String>();
		
		for(int i = 0; i < imageNames.size(); i++)
		{
			preparedStatement = connection.prepareStatement("INSERT INTO images VALUES(?,?)");
			
			preparedStatement.setInt(1, 0);
			preparedStatement.setInt(2, idProduct);
			
			preparedStatement.executeUpdate();
			
			ResultSet rs = statement.executeQuery("SELECT last_insert_id()");
			
			rs.next();
			imageIds.put(imageNames.get(i), Integer.toString(rs.getInt(1)));
		}
		
		disconnect();
		
		return imageIds;
	}

/* FOR UPDATING/DELETE
 * --------------------------------------------------------------------------------------------------------
 * FOR UPDATING/DELETE
 */
	//A method for updating a product in database
	public int updateProduct(Product product) throws SQLException
	{
		connect();
			
		preparedStatement = connection.prepareStatement("UPDATE products SET name = ?, description = ?, "
				+ "price = ?, stock = ?, categoryOf = ?, typeOf = ?, conditionOf = ? WHERE idProducts = ?");
		
		preparedStatement.setString(1, product.getName());
		preparedStatement.setString(2,  product.getDescription());
		preparedStatement.setFloat(3, product.getPrice());
		preparedStatement.setInt(4, product.getStock());
		preparedStatement.setString(5, product.getCategory());
		preparedStatement.setString(6, product.getType());
		preparedStatement.setString(7, product.getCondition());
		preparedStatement.setInt(8, product.getIdProduct());
		
		preparedStatement.executeUpdate();

		ResultSet rs = statement.executeQuery("SELECT last_insert_id()");
		rs.next();
		int lastid = rs.getInt(1);
		
		disconnect();
		
		return lastid;
	}
	//A method for updating tags in database
	public void updateTags(int idProduct, ArrayList<String>tags) throws SQLException
	{
		connect();
		
		if(tags.isEmpty())
			return;
		
		preparedStatement = connection.prepareStatement("UPDATE tags SET shooter = ?, rpg = ?, "
				+ "adventure = ?, racing = ? WHERE idProducts = ?");
			
		preparedStatement.setInt(5, idProduct);
		
		//Set all to false
		preparedStatement.setBoolean(1, false);
		preparedStatement.setBoolean(2, false);
		preparedStatement.setBoolean(3, false);
		preparedStatement.setBoolean(4, false);
		
		//Set selected to true
		for(String s: tags){
			if(s.compareTo("shooter") == 0)preparedStatement.setBoolean(1, true);
			if(s.compareTo("rpg") == 0)preparedStatement.setBoolean(2, true);
			if(s.compareTo("action/adventure") == 0)preparedStatement.setBoolean(3, true);
			if(s.compareTo("racing") == 0)preparedStatement.setBoolean(4, true);
		}
		
		preparedStatement.executeUpdate();
		
		disconnect();
	}
	//A method for deleting images by id within the database
	public void deleteImagesByImageId(ArrayList<String> imageIds) throws SQLException
	{
		connect();
		for(String s: imageIds)
		{
			statement.executeUpdate("DELETE FROM  images WHERE imageId = " + s);
		}
		disconnect();
	}
	//A method to delete products from database
	public void deleteProduct(String id) throws SQLException
	{
		connect();
		
		ResultSet rs = statement.executeQuery(
				"SELECT count(*) FROM products WHERE idProducts = " + id);
			boolean pre = rs.next();
			if(! pre)
			{
				throw new RuntimeException("The product does not exist!");
			}

			statement.executeUpdate("DELETE FROM products WHERE idProducts = " + id);
		
		disconnect();
	}
	
/* FOR FINDING
 * --------------------------------------------------------------------------------------------------------
 * FOR FINDING
 */
	
	//A method for returning all products within database
	public ArrayList<Product> findAllProducts() throws SQLException
	{
		connect();

		String queryString = "SELECT * FROM products";
		ResultSet rs = statement.executeQuery(queryString);

		ArrayList<Product> products = new ArrayList<Product>();

		while (rs.next())
		{
			int id = rs.getInt(1);
			String name = rs.getString(2);
			String description = rs.getString(3);
			float price = rs.getFloat(4);
			int stock = rs.getInt(5);
			String categoryOf = rs.getString(6);
			String typeOf = rs.getString(7);
			String conditionOf = rs.getString(8);
			java.sql.Date creationDate = rs.getDate(9);

			products.add(new Product(id, name, description, price, stock, categoryOf,  typeOf, conditionOf, creationDate));
		}

		disconnect();
		return products;
	}
	
	//A method for finding products within database
	public ArrayList<Product> findProducts(String queryName) throws SQLException
	{
		connect();

		String queryString = "SELECT * FROM products"
							+ " WHERE name LIKE '%"+ queryName +"%'";
		ResultSet rs = statement.executeQuery(queryString);

		ArrayList<Product> products = new ArrayList<Product>();

		while (rs.next())
		{
			int id = rs.getInt(1);
			String name = rs.getString(2);
			String description = rs.getString(3);
			float price = rs.getFloat(4);
			int stock = rs.getInt(5);
			String categoryOf = rs.getString(6);
			String typeOf = rs.getString(7);
			String conditionOf = rs.getString(8);
			java.sql.Date creationDate = rs.getDate(9);

			products.add(new Product(id, name, description, price, stock, categoryOf,  typeOf, conditionOf, creationDate));
		}

		disconnect();
		return products;
	}
	
	//A method for finding one product based on id within the database
	public Product findProduct(int idQuery) throws SQLException
	{
		connect();

		String queryString = "SELECT * FROM products"
							+ " WHERE idProducts ="+idQuery;
		ResultSet rs = statement.executeQuery(queryString);

		Product product = null;

		if(rs.next()){
			int id = rs.getInt(1);
			String name = rs.getString(2);
			String description = rs.getString(3);
			float price = rs.getFloat(4);
			int stock = rs.getInt(5);
			String categoryOf = rs.getString(6);
			String typeOf = rs.getString(7);
			String conditionOf = rs.getString(8);
			java.sql.Date creationDate = rs.getDate(9);

			product = new Product(id, name, description, price, stock, categoryOf,  typeOf, conditionOf, creationDate);
		}

		disconnect();
		return product;
	}
	
	//A method for finding one product based on id within the database
	public ArrayList<Product> findProductsById(String idArray) throws SQLException
	{
		connect();
		
		String sqlInjection = "AND products.idProducts IN(" + idArray + ")";
				
		System.out.println(sqlInjection) ;
		String queryString = "SELECT * FROM products"
							+ " JOIN tags ON tags.idProducts = products.idProducts "
							+ sqlInjection;
		System.out.println(queryString) ;
		ResultSet rs = statement.executeQuery(queryString);

		ArrayList<Product> products = new ArrayList<Product>();
		
		while(rs.next()){
			int id = rs.getInt(1);
			System.out.println(id);
			String name = rs.getString(2);
			String description = rs.getString(3);
			float price = rs.getFloat(4);
			int stock = rs.getInt(5);
			String categoryOf = rs.getString(6);
			String typeOf = rs.getString(7);
			String conditionOf = rs.getString(8);
			java.sql.Date creationDate = rs.getDate(9);
			ArrayList<String> tags = new ArrayList<String>();
			tags.add(rs.getString(11));
			tags.add(rs.getString(12));
			tags.add(rs.getString(13));
			tags.add(rs.getString(14));

			products.add(new Product(id, name, description, price, stock, categoryOf,  typeOf, conditionOf, creationDate, tags));
		}

		disconnect();
		return products;
	}
	
	//A method for finding image details within database
	public ArrayList<Integer> findImages(int productId) throws SQLException
	{
		connect();

		String queryString = "SELECT * FROM images"
							+ " WHERE idProducts = '"+ productId +"'";
		ResultSet rs = statement.executeQuery(queryString);

		ArrayList<Integer> imageIds = new ArrayList<Integer>();

		while (rs.next())
		{
			imageIds.add(rs.getInt(1));
		}

		disconnect();
		return imageIds;
	}
	
	//A method for finding profile image details within database
	public int findProfileImage(int productId) throws SQLException
	{
		connect();
		int imageId = 0;
		String queryString = "SELECT * FROM images"
							+ " WHERE idProducts = '"+ productId +"'";
		ResultSet rs = statement.executeQuery(queryString);

		if(rs.next())
			imageId = rs.getInt(1);

		disconnect();
		return imageId;
	}
	//A method for finding tags within database
	public ArrayList<String> findTags(int idProduct) throws SQLException
	{
		connect();
		
		ArrayList<String> tags = new ArrayList<String>();
		
		ResultSet rs = statement.executeQuery("SELECT * FROM tags "
				+ "WHERE idProducts = "+idProduct+"");
			
		if(rs.next()){
			 tags.add(rs.getString(2));
			 tags.add(rs.getString(3));
			 tags.add(rs.getString(4));
			 tags.add(rs.getString(5));
		}
		
		disconnect();
		return tags;
	}
	//An advanced search methods with many options for both admin and customer search
	//For retrieving products from database
	public ArrayList<Product> advancedSearch(String query, String category, String type, String condition, String tagsShooter, String tagsRpg, String tagsAdventure, String tagsRacing) throws SQLException
	{
		connect();
		ArrayList<Product> products = new ArrayList<Product>();
		
		String splice = "";
		
		if(!category.equals("any"))
			splice += "AND categoryOf = '"+category+"' ";
		if(!condition.equals("any"))
			splice += "AND conditionOf = '"+condition+"' ";
		if(!type.equals("any"))
			splice += "AND typeOf = '"+type+"' ";
		if(tagsShooter.equals("true"))
			splice += "AND shooter = '1' ";
		if(tagsRpg.equals("true"))
			splice += "AND rpg = '1' ";
		if(tagsAdventure.equals("true"))
			splice += "AND adventure = '1' ";
		if(tagsRacing.equals("true"))
			splice += "AND racing = '1' ";
		
		ResultSet rs = statement.executeQuery("SELECT * FROM products"
				+ " JOIN tags ON tags.idProducts = products.idProducts AND name LIKE '%"+query+"%' "+splice);
		
		while(rs.next()){
			int id = rs.getInt(1);
			String name = rs.getString(2);
			String description = rs.getString(3);
			float price = rs.getFloat(4);
			int stock = rs.getInt(5);
			String categoryOf = rs.getString(6);
			String typeOf = rs.getString(7);
			String conditionOf = rs.getString(8);
			java.sql.Date creationDate = rs.getDate(9);
			ArrayList<String> tags = new ArrayList<String>();
			tags.add(rs.getString(11));
			tags.add(rs.getString(12));
			tags.add(rs.getString(13));
			tags.add(rs.getString(14));

			products.add(new Product(id, name, description, price, stock, categoryOf,  typeOf, conditionOf, creationDate, tags));
		}
		
		disconnect();
		return products;
	}
}
