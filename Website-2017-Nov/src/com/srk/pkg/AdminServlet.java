package com.srk.pkg;

import java.io.*;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import com.google.gson.Gson;

import java.lang.Object;

/**
 * Servlet implementation class AdminServlet
 */
public class AdminServlet extends HttpServlet {
	
	 private static final long serialVersionUID = 1L;
	 private JdbcUtility jdbc = new JdbcUtility();
	 private Gson gson = new Gson();
	 private ServletUtility servletUtility = new ServletUtility();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	 public  AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	 //'Get' for all manners of retrieving products
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		
		//Variable declaration
		PrintWriter out = response.getWriter();
		ArrayList<Product> products = null;
		Product product = null;
		String id = request.getParameter("id");
				
		if(id == null){
			String query = request.getParameter("query");
			String category = request.getParameter("category");
			String type = request.getParameter("type");
			String condition = request.getParameter("condition");
			String tagsShooter = request.getParameter("shooter");
			String tagsRpg = request.getParameter("rpg");
			String tagsAdventure = request.getParameter("adventure");
			String tagsRacing = request.getParameter("racing");
			
			try{
				products = jdbc.advancedSearch(query, category, type, condition, tagsShooter, tagsRpg, tagsAdventure, tagsRacing);
			}catch(Exception ex){
				ex.printStackTrace(out);
			}
		}else{//Finding and returning ONE product	
			try{
				product = jdbc.findProduct(Integer.parseInt(id));
				List<String> images = servletUtility.getImages(Integer.parseInt(id),out);
				product.setImages(images);	
				product.setTags(jdbc.findTags(Integer.parseInt(id)));
			}catch(Exception ex){
				ex.printStackTrace(out);
			}
		}
		//Output
		if(products != null){
			for(Product p: products)
			{
				String imageName = servletUtility.getProfileImage(p.getIdProduct(), out);
				p.setProfileImage(imageName);
			}
			out.println(gson.toJson(products));
		}else{
			product.setProfileImage(product.getImages().size() > 0 ? product.getImages().get(0) : null);
			out.println(gson.toJson(product));
		}
		
		out.close();
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	//'Post' for creating product
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String filePath = getServletContext().getRealPath("/images");
		
		List<FileItem> formItems = servletUtility.getFormItems(request,  out);

		int productId = servletUtility.addProduct(formItems, out);
		if(ServletFileUpload.isMultipartContent(request))
		{
			servletUtility.saveFiles(formItems, productId, out, filePath);
		}
		out.println(productId);
		out.close();
	}

	/**
	 * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
	 */
	//'Put' for updating product
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String filePath = getServletContext().getRealPath("/images");
		
		List<FileItem> formItems = servletUtility.getFormItems(request,  out);

		int productId = servletUtility.updateProduct(formItems, out);
		servletUtility.deleteFiles(formItems, Integer.toString(productId), out, filePath);

		servletUtility.saveFiles(formItems, productId, out, filePath);
		out.close();
	}

	/**
	 * @see HttpServlet#doDelete(HttpServletRequest, HttpServletResponse)
	 */
	//'Delete' for deleting products
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String filePath = getServletContext().getRealPath("/images");
		
		String id = request.getParameter("id");
		try{
			jdbc.deleteProduct(id);
		}catch(Exception ex){
			ex.printStackTrace(out);
		}
		servletUtility.deleteAllFiles(id, out, filePath);
		out.println(id);
		out.close();
	}
}
